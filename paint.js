const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");
const color = document.getElementById("color");
const linewidth = document.getElementById("linewidth");
let buttonId = "";
let posMouse;
let clicNum = 0;
let isDrawing = false;
let currentColor = "black";
let currentLineWidth = 1;

currentColor = localStorage.getItem('color');
currentLineWidth = localStorage.getItem('linewidth');
ctx.lineWidth = currentLineWidth;

const buttonToolbar = document.querySelectorAll("#toolbar > button");
buttonToolbar.forEach(button => {
    button.onclick = () => {
        buttonToolbar.forEach(button => {
            button.classList.remove("selected");
        })

        button.classList.add("selected");
        console.log("Voici l'id du bouton cliqué : ", button.id);

        buttonId = button.id;
    }
});

function getMousePos(canvas, evt) {
    const rect = canvas.getBoundingClientRect();
    return {
        // Il vaut mieux avoir des entiers quand on travail avec le canvas
        x: parseInt(evt.clientX) - parseInt(rect.left),
        y: parseInt(evt.clientY) - parseInt(rect.top)
    };
};

// canvas.onclick = (evt) => {
//     const pos = getMousePos(canvas, evt);
//     ctx.strokeStyle = currentColor;

//     if (clicNum === 0) {
//         switch (buttonId) {
//             case "line":
//                 posMouse = pos;
//                 clicNum++;
//                 break;

//             case "rectangle":
//                 posMouse = pos;
//                 clicNum++;
//                 break;

//             case "circle":
//                 posMouse = pos;
//                 clicNum++;
//                 break;

//             default:
//                 break;
//         }

// if (buttonId === "rectangle") {
//     posMouse = pos;
//     clicNum++;
// } else if (buttonId === "line") {
//     posMouse = pos;
//     console.log(posMouse);
//     clicNum++;
// }

// } else {
//     switch (buttonId) {
//         case "line":
//             ctx.beginPath();
//             ctx.moveTo(pos.x, pos.y);
//             ctx.lineTo(posMouse.x, posMouse.y);
//             ctx.stroke();
//             ctx.closePath();
//             clicNum = 0;
//             break;

//         case "rectangle":
//             ctx.strokeRect(posMouse.x, posMouse.y, pos.x - posMouse.x, pos.y - posMouse.y);
//             clicNum = 0;
//             break;

//         case "circle":
//             const centerX = (pos.x + posMouse.x) / 2;
//             const centerY = (pos.y + posMouse.y) / 2;
//             const radiusCircle = Math.max(Math.abs(pos.x - posMouse.x), Math.abs(pos.y - posMouse.y)) / 2;

//             ctx.beginPath();
//             ctx.arc(centerX, centerY, radiusCircle, 0, Math.PI * 2);
//             ctx.stroke();
//             ctx.closePath();
//             clicNum = 0;
//             break;

//         default:
//             break;
//     }

// if (buttonId === "rectangle") {
//     const width = pos.x - posMouse.x;
//     const height = pos.y - posMouse.y;
//     ctx.strokeRect(posMouse.x, posMouse.y, width, height);
//     clicNum = 0;
// } else if (buttonId === "line") {
//     ctx.beginPath();
//     ctx.moveTo(pos.x, pos.y);
//     ctx.lineTo(posMouse.x, posMouse.y);
//     ctx.stroke();
//     ctx.closePath();
//     console.log(posMouse);
//     clicNum = 0;
// }
//     }
// };


function redraw() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}


window.addEventListener('mousedown', evt => {
    const pos = getMousePos(canvas, evt);
    posMouse = pos;
    ctx.strokeStyle = currentColor;
    isDrawing = true;

    switch (buttonId) {
        case "pencil":
        case "eraser":
            ctx.globalCompositeOperation = 'source-over';

            if (buttonId === "eraser") {
                ctx.globalCompositeOperation = 'destination-out';
            }

            ctx.beginPath();
            ctx.moveTo(pos.x, pos.y);
            break;
    }


    // if (buttonId === "pencil" || buttonId === "eraser") {
    //     const pos = getMousePos(canvas, evt);
    //     ctx.strokeStyle = currentColor;
    //     ctx.beginPath();
    //     ctx.moveTo(pos.x, pos.y);
    //     ctx.globalCompositeOperation = 'source-over';

    //     if (buttonId === "pencil") {
    //         isDrawing = true;
    //     } else if (buttonId === "eraser") {
    //         isErasing = true;
    //         ctx.globalCompositeOperation = 'destination-out';
    //     }
    // }
});

window.addEventListener('mousemove', evt => {
    const pos = getMousePos(canvas, evt);
    redraw();

    if (isDrawing === true) {
        switch (buttonId) {
            case "pencil":
            case "eraser":
                ctx.lineTo(pos.x, pos.y);
                ctx.stroke();
                break;

            case "line":
                ctx.beginPath();
                ctx.moveTo(posMouse.x, posMouse.y);
                ctx.lineTo(pos.x, pos.y);
                ctx.stroke();
                ctx.closePath();
                break;

            case "rectangle":
                ctx.strokeRect(posMouse.x, posMouse.y, pos.x - posMouse.x, pos.y - posMouse.y);
                break;

            case "circle":
                const centerX = (pos.x + posMouse.x) / 2;
                const centerY = (pos.y + posMouse.y) / 2;
                const radiusCircle = Math.max(Math.abs(pos.x - posMouse.x), Math.abs(pos.y - posMouse.y)) / 2;
    
                ctx.beginPath();
                ctx.arc(centerX, centerY, radiusCircle, 0, Math.PI * 2);
                ctx.stroke();
                ctx.closePath();
                break;
        }
    }


    // if (buttonId === "pencil" || buttonId === "eraser") {
    //     const pos = getMousePos(canvas, evt);
    //     if (isDrawing === true) {
    //         ctx.lineTo(pos.x, pos.y);
    //         ctx.stroke();
    //     }
    // }
});

window.addEventListener('mouseup', evt => {
    isDrawing = false;

    switch (buttonId) {
        case "pencil":
        case "eraser":
            ctx.closePath();
            if (buttonId === "eraser") {
                ctx.globalCompositeOperation = 'source-over';
            }
            break;
    }


    // if (buttonId === "pencil" || buttonId === "eraser") {
    //     if (isDrawing === true) {
    //         ctx.closePath();
    //         isDrawing = false;

    //         if (buttonId === "eraser") {
    //             ctx.globalCompositeOperation = 'source-over';
    //         }
    //     }
    // }
});


// gestion gomme
// window.addEventListener('mousedown', evt => {
//     if (buttonId === "eraser") {
//         const pos = getMousePos(canvas, evt);
//         ctx.beginPath();
//         ctx.moveTo(pos.x, pos.y);
//         isErasing = true;
//         ctx.globalCompositeOperation = 'destination-out';
//     }
// });

// window.addEventListener('mousemove', evt => {
//     if (buttonId === "eraser") {
//         const pos = getMousePos(canvas, evt);
//         if (isErasing === true) {
//             ctx.lineTo(pos.x, pos.y);
//             ctx.stroke();
//         }
//     }
// });

// window.addEventListener('mouseup', evt => {
//     if (buttonId === "eraser") {
//         if (isErasing === true) {
//             ctx.closePath();
//             isErasing = false;
//             ctx.globalCompositeOperation = 'source-over';
//         }
//     }
// });


// gestion couleur
color.addEventListener('change', evt => {
    currentColor = evt.target.value;
    localStorage.setItem('color', currentColor);
});


// gestion taille ligne
linewidth.addEventListener('change', evt => {
    currentLineWidth = evt.target.value;
    ctx.lineWidth = currentLineWidth;
    localStorage.setItem('linewidth', currentLineWidth);
})